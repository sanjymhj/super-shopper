from django.conf.urls import include, url, patterns
from .views import CartListView, ajaxAddToCart
urlpatterns = patterns(
	"",
    url(r'^$', CartListView.as_view(),name="cart"),
    url(r'^ajax/addToCart/$', ajaxAddToCart),
 	   
)