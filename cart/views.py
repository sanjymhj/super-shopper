from django.shortcuts import render
from django.views.generic import ListView
from .models import Cart
from product.models import Product
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json

# Create your views here.


class CartListView(ListView):

	model = Cart
	template_name = "cart.html"
	def get_queryset(self):
 		return Cart.objects.filter(user = self.request.user)

				


@csrf_exempt
def ajaxAddToCart(request):
	if(request.method == 'POST'):
		print request.POST
		cart, isCreated = Cart.objects.get_or_create(user = request.user, product_id = request.POST['product_id'])
	
		if isCreated:
			cart.quantity = request.POST['quantity']
		else:
			cart.quantity = cart.quantity + int(request.POST['quantity'])

		cart.save()



		response_data = {}
		data = {
				'cart':cart.id
				}
		response_data['result'] = 'Sucess'
		response_data['data'] = data

		return HttpResponse(json.dumps(response_data), content_type='application/json')
