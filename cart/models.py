from django.db import models
from django.contrib.auth.models import User
from product.models import Product

# Create your models here.


class Cart(models.Model):
	user = models.ForeignKey(User)
	product = models.ForeignKey(Product)
	quantity = models.IntegerField(default=0)

	created_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "%s  > %s > %s" %(self.user, self.product, self.quantity)
