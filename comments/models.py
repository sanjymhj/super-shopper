from django.db import models
from account.models import Account
from product.models import Product
from django.db.models import Sum, Avg

RATING_CHOICE = [(0,0),(1,1),(2,2),(3,3),(4,4),(5,5)]

# Create your models here.
class Comments(models.Model):
	user = models.ForeignKey(Account)
	product = models.ForeignKey(Product)


	comments = models.TextField(max_length=200, null=True, blank=True)
	rating = models.IntegerField(choices=RATING_CHOICE, default=0)

	created_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)

	@staticmethod
	def average_rating(self):
		all_comments = Comments.objects.filter(rating__gte =0)
		avg_rating = all_comments.aggregate(Avg('rating'))
		return avg_rating['rating__avg']

	def __str__(self):
		return "%s  > %s > %s" %(self.user, self.product, self.comments)

