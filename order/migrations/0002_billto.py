# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BillTo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('companyName', models.CharField(max_length=255, null=True, blank=True)),
                ('email', models.EmailField(max_length=255)),
                ('firstName', models.CharField(max_length=50)),
                ('middleName', models.CharField(max_length=50, null=True, blank=True)),
                ('lastName', models.CharField(max_length=50)),
                ('gender', models.CharField(default=b'M', max_length=6, choices=[(b'M', b'MALE'), (b'F', b'FEMALE'), (b'O', b'OTHERS')])),
                ('address1', models.CharField(max_length=100)),
                ('address2', models.CharField(max_length=100, null=True, blank=True)),
                ('country', models.CharField(max_length=100)),
                ('district', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=10, unique=True, null=True, blank=True)),
                ('order', models.ForeignKey(to='order.Order')),
            ],
        ),
    ]
