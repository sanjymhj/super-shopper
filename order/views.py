from django.shortcuts import render
from django.views.generic import TemplateView
from cart.models import Cart
# Create your views here.


class OrderView(TemplateView):
	template_name = "checkout.html"
	model = Cart

	def get_context_data(self, **kwargs):
		context = super(OrderView, self).get_context_data(**kwargs)
		context['cart_list'] = Cart.objects.filter(user=self.request.user)
		return context

	
				