from django.db import models

from django.contrib.auth.models import User

# Create your models here.

GENDER_CHOICE=[('M','MALE'),
    ('F','FEMALE'),
    ('O','OTHERS')
    ]



class Order(models.Model):
	user = models.ForeignKey(User)
	tax = models.IntegerField();
	shippingCharge = models.IntegerField()
	discount = models.IntegerField()

	



	def __str__(self):
		return self.user.username

class OrderedItem(models.Model):
	order = models.ForeignKey(Order)
	name = models.CharField(max_length=255, blank=False, null=False)
	price = models.DecimalField(decimal_places=2, max_digits=11)
	qty = models.IntegerField()
	
	manufacturer = models.CharField(max_length=255, blank=False, null=False)

	created_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "%s %s %s" %(self.order.user.username, self.name,self.modified_date)


class BillTo(models.Model):
	order = models.ForeignKey(Order)
	companyName = models.CharField(max_length = 255, blank = True, null = True)
	email = models.EmailField(max_length=255, blank = False, null = False);
	firstName = models.CharField(max_length = 50, blank = False, null = False);

	middleName = models.CharField(max_length = 50, blank = True, null = True);
	lastName = models.CharField(max_length = 50, blank = False, null = False);
	gender = models.CharField(choices=GENDER_CHOICE, max_length=6, default='M')
	address1 = models.CharField(max_length=100)
	address2 = models.CharField(max_length=100, null=True, blank=True)
	country = models.CharField(max_length = 100, blank= False, null = False)

	district = models.CharField(max_length = 100, blank = False,null=False)
	phone = models.CharField(max_length = 10, unique=True, blank=True, null=True)
	
