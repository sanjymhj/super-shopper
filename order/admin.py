from django.contrib import admin
from .models import OrderedItem, Order,BillTo

# Register your models here.


class OrderInline(admin.TabularInline):
	model = OrderedItem
	extra = 0

class BillToInline(admin.TabularInline):
	model = BillTo
	extra = 0

class OrderAdmin(admin.ModelAdmin):
	inlines = [OrderInline, BillToInline]

admin.site.register(Order, OrderAdmin)
