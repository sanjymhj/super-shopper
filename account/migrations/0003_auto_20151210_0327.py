# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_fix_str'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='address',
            field=models.CharField(default='Kathmandu', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='account',
            name='address2',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='phone',
            field=models.CharField(max_length=10, unique=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='photo',
            field=models.ImageField(null=True, upload_to='profile', blank=True),
        ),
        migrations.AddField(
            model_name='account',
            name='rating',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='account',
            name='slug',
            field=models.SlugField(default='user-2015-12-10 03:27:13.060004+00:00', unique=True),
            preserve_default=False,
        ),
    ]
