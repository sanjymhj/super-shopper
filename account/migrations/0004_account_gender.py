# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20151210_0327'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='gender',
            field=models.CharField(default='M', max_length=6, choices=[('M', 'MALE'), ('F', 'FEMALE'), ('O', 'OTHERS')]),
        ),
    ]
