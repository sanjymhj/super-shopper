# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_account_gender'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='slug',
        ),
    ]
