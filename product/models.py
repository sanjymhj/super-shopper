from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
# Create your models here.


DISTRICT_CHOICE=[('KTM','Kathmandu'),
	('LAL','Lalitpur'),
	('BKT','Bhaktapur')
	]

class Location(models.Model):
	district = models.CharField(choices=DISTRICT_CHOICE, max_length=50)
	village = models.CharField(max_length=100)
	ward_no = models.CharField(max_length=100)
	slug = models.SlugField(unique=True)


	def __str__(self):
		return "%s %s %s" %(self.district, self.village, self.ward_no)


class Category(MPTTModel):
	name = models.CharField(max_length=200)
	slug = models.SlugField(unique=True)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

class MPTTMeta:
    	order_insertion_by = ['name']

def __str__(self):
        return self.name

class Product(models.Model):
	name = models.CharField(max_length=255, blank=False, null=False)
	slug = models.SlugField(unique=True)
	description = models.TextField()
	price = models.DecimalField(decimal_places=2, max_digits=11)
	qty = models.IntegerField()
	#photo = models.ImageField(upload_to='product',blank=True, null=True)

	
	location = models.ForeignKey(Location)
	manufacturer = models.ForeignKey(User)
	category = TreeForeignKey('Category', null=True, blank=True, db_index=True)


	def get_averageRating(self):
		from comments.models import Comments
		avg_rating = Comments.average_rating(self)
		return avg_rating


	def get_manufacturer_email(self):
		return self.manufacturer.email
	get_manufacturer_email.short_description='Email'#change the header in the table

	def __str__(self):
		return self.name

class ProductImage(models.Model):
    photo = models.ImageField(upload_to='Product_images',blank = True, null = True)
    obj = models.ForeignKey(Product)

    
