from django import template

register = template.Library()

@register.inclusion_tag('product/product_category.html')
def show_category(category):
	tax = 100 * 2; 
	return {'category_list': category, 'tax': tax}