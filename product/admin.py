from django.contrib import admin
from comments.models import Comments
# Register your models here.
from .models import Category, Location, Product, ProductImage
from mptt.admin import MPTTModelAdmin

class ImageInline(admin.StackedInline):
	model = ProductImage

class CommentsInline(admin.TabularInline):
	model = Comments
	extra = 0

class ProductAdmin(admin.ModelAdmin):
	inlines = [ImageInline, CommentsInline]
	list_display=['name','price','qty','location','get_manufacturer_email']
	prepopulated_fields={'slug':('name',),}
	

class CategoryAdmin(MPTTModelAdmin):
	fields = ['name','slug', 'parent']
	list_display =('name', )

	def __str__(self):
		return self.fields




admin.site.register(Category, CategoryAdmin)
admin.site.register(Location)
admin.site.register(Product, ProductAdmin)
