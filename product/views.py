import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from django.views.decorators.csrf import csrf_exempt


from .models import Product, Category
from comments.models import Comments
# Create your views here.
class ProductListView(ListView):
	model=Product
	#template_name='base.html'
	

class ProductDetailView(DetailView):
	model=Product
	#template_name='base.html'
	def get_context_data(self, **kwargs):
		context = super(ProductDetailView, self).get_context_data(**kwargs)
		context['category_list'] = Category.objects.all()#filter(parent__isnull=True)
		
		return context

class HomeView(TemplateView):
	template_name='product/home.html'
	model = ProductDetailView
	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['product_list'] = Product.objects.filter()[:2]
		#context['featured_items'] = Product.objects.all()[:2]
		return context

@csrf_exempt
def ajaxCommentForm(request):
	if(request.method == 'POST'):
		comment = Comments(
			user_id = request.user.id,
			product_id = request.POST['product_id'],
			comments = request.POST['comment'],
			rating = request.POST['rating']
			)
		comment.save()
		response_data = {}
		mydata = {
				'comment_id':comment.id,
				'time':comment.created_date.strftime("%H - %M %P"),
				'date':comment.created_date.strftime("%Y / %b / %d"),
				'comment_user':comment.user.user.username,
				'comment_text':comment.comments,
				'rating': comment.rating
				}
		response_data['result'] = 'Sucess'
		response_data['data'] = mydata

		return HttpResponse(json.dumps(response_data), content_type='application/json')
